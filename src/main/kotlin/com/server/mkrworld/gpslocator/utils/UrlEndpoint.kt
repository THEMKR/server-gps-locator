package com.server.mkrworld.gpslocator.utils

/**
 * @author THEMKR
 * Class to hold all the API Endpoint
 */
interface UrlEndpoint {
    /**
     * Enum contain all the url access by VertX Controller
     */
    interface VertX {
        companion object {
            const val GET_GPS_LOCATION = "/gpslocator/find/location/"
        }
    }
}