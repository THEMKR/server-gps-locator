package com.server.mkrworld.gpslocator.utils

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.server.mkrworld.gpslocator.exception.DataParsingException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.PrintWriter
import java.io.StringWriter

/**
 * @author THEMKR
 * Class to hold the Json Converter Utils method
 */
class Converter {
    companion object {

        private val LOGGER: Logger = LoggerFactory.getLogger(Converter::class.java)
        private val MAPPER = ObjectMapper()

        /**
         * Method to parse the Object into JsonString
         * @param obj Any Object
         */
        fun toString(obj: Any): String {
            return try {
                MAPPER.writeValueAsString(obj)
            } catch (e: JsonProcessingException) {
                val message = String.format(Constants.ErrorMessage.PARSING_DTO_TO_JSON_S2.message, obj::class.java.canonicalName, getStackTrace(e))
                LOGGER.error(message).toString()
                throw DataParsingException(message)
            }
        }

        /**
         * Method to convert the JSON-String in its correspond DTO
         */
        fun <MKR> toObject(json: String): MKR? {
            return try {
                MAPPER.readValue(json, object : TypeReference<MKR>() {})
            } catch (e: Exception) {
                val message = String.format(Constants.ErrorMessage.PARSING_JSON_TO_DTO_S2.message, object : TypeReference<MKR>() {}.javaClass.canonicalName, getStackTrace(e))
                LOGGER.error(message)
                throw DataParsingException(message)
            }
        }

        /**
         * Method to get the stack trace from the Throwable Object
         * @param throwable
         */
        fun getStackTrace(throwable: Throwable): String? {
            val sw = StringWriter()
            val pw = PrintWriter(sw, true)
            throwable.printStackTrace(pw)
            return sw.buffer.toString()
        }
    }
}