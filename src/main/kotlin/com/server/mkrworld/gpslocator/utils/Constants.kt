package com.server.mkrworld.gpslocator.utils

interface Constants {
    companion object {
        val ADDRESS_FIND_LOCATION: String = "ADDRESS_FIND_LOCATION"
    }

    /**
     * Enum to hold the type of GateWay
     */
    enum class GatewayType {
        VERTX,
        SPRING_CONTROLLER
    }

    /**
     * Enum to hold the method type of the request
     */
    enum class RequestType {
        GET,
        PUT
    }

    /**
     * Enum contain the error message publish by system
     * S : Number of String Param receive at the time of formatting
     */
    enum class ErrorMessage {
        PARSING_JSON_TO_DTO_S2("Error in json to convert into DTO. :: class %s :: trace %s"),
        PARSING_DTO_TO_JSON_S2("Error in data to convert into JSON. :: class %s :: trace %s"),
        MULTIPLE_API_SUBSCRIBER_S2("Multiple subscriber of @API with same parameter are exist in the system :: class-1 %s :: class-2 %s"),
        API_NOT_FOUND_S1("Unable to find @API  :: %s");

        val message: String

        constructor(message: String) {
            this.message = message
        }
    }
}