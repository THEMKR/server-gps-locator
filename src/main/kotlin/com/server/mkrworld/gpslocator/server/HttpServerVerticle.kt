package com.mkrworld.server.gpslocator.server

import com.server.mkrworld.gpslocator.api.Router
import com.server.mkrworld.gpslocator.config.AppConfig
import com.server.mkrworld.gpslocator.utils.Converter
import io.vertx.core.AbstractVerticle
import io.vertx.core.http.HttpServer
import io.vertx.core.http.HttpServerRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


/**
 * @author THEMKR
 * Create Http Server
 */
@Service
class HttpServerVerticle : AbstractVerticle() {

    @Autowired
    private var appConfig: AppConfig? = null

    @Autowired
    private var router: Router? = null

    private var httpServer: HttpServer? = null

    @Throws(Exception::class)
    override fun start() {
        httpServer = vertx.createHttpServer()
        httpServer?.requestHandler { request: HttpServerRequest ->
            System.out.println("incoming request! : ${request.method()} : ${request?.uri()} : ${request?.path()} : ${request?.params().toList()}")
            try {
                val baseResponse = router!!.vertXRequest(request)
                val response = request.response()
                response.statusCode = baseResponse.status;
                response.putHeader("Content-Type", "application/json")
                response.end(Converter.toString(baseResponse))
            } catch (e: Exception) {
                val response = request.response()
                response.statusCode = 500
                response.end("ERROR : ${Converter.getStackTrace(e)}")
            }
        }
        httpServer?.listen(appConfig?.vertxPort ?: 8001)
    }

//    when (request.method()) {
//        HttpMethod.PUT -> {
//            val fullRequestBody = Buffer.buffer()
//            request.handler { buffer: Buffer? ->
//                fullRequestBody.appendBuffer(buffer)
//            }
//            request.endHandler { buffer ->
//                println("REQUEST  : ${fullRequestBody::class.java.name} :  ${fullRequestBody.getString(0, fullRequestBody.length())} : ${buffer}")
//            }
//        }
//        HttpMethod.GET -> {
//            val currentTimeMillis = System.currentTimeMillis()
//            val gpsLocation = GpsLocation()
//            gpsLocation.key = request?.params().get("id") ?: "${currentTimeMillis}"
//            gpsLocation.lat = currentTimeMillis.toDouble()
//            gpsLocation.long = currentTimeMillis.toDouble()
//            gpsLocation.updatedAt = Date()
//            dataBase?.gpsLocationRepo?.save(gpsLocation)
//            val findGpsLocationById = dataBase?.gpsLocationRepo?.findAll()
//            println("REQUEST  : $findGpsLocationById")
//        }
//    }
}