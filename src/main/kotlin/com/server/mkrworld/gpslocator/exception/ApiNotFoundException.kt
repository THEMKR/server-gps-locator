package com.server.mkrworld.gpslocator.exception

/**
 * @author THEMKR
 * Exception throw in case when we are unable to serialize/deserialize the Data
 */
class ApiNotFoundException(message: String) : RuntimeException(message) {

}