package com.server.mkrworld.gpslocator.exception

/**
 * @author THEMKR
 * Exception throw in case when something wrong occur at the time of initialization of the system
 */
class InitializeException(message: String) : RuntimeException(message) {

}