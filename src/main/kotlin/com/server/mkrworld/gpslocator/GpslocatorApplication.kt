package com.server.mkrworld.gpslocator

import com.mkrworld.server.gpslocator.server.HttpServerVerticle
import io.vertx.core.AsyncResult
import io.vertx.core.Vertx
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import javax.annotation.PostConstruct

@SpringBootApplication
@ComponentScan("com.server.mkrworld.gpslocator")
class GpslocatorApplication {

    @Autowired
    private var httpServerVerticle: HttpServerVerticle? = null

    /**
     * Method run after the successful initialization of Application
     */
    @PostConstruct
    fun initVerticle() {
        Vertx.vertx().deployVerticle(httpServerVerticle) { asyncResult: AsyncResult<String>? -> System.out.println("started! -------- ${asyncResult}"); }
    }
}

fun main(args: Array<String>) {
    runApplication<GpslocatorApplication>(*args)
}
