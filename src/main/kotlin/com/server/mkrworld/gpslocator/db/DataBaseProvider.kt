package com.server.mkrworld.gpslocator.db

import com.server.mkrworld.gpslocator.db.repo.GpsLocationRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * @author THEMKR
 * Class to handle all the REPO
 */
@Service
class DataBaseProvider {

    @Autowired
    lateinit var gpsLocationRepo: GpsLocationRepo
}