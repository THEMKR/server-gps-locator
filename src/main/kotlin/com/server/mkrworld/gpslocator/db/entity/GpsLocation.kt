package com.server.mkrworld.gpslocator.db.entity

import com.server.mkrworld.gpslocator.utils.Converter
import lombok.NoArgsConstructor
import java.io.Serializable
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * @author THEMKR
 * Entity class to hold the Mapping of gps_location Table in gps_location DB
 */
@NoArgsConstructor
@Entity
@Table(name = "location")
class GpsLocation : Serializable {

    @Id
    @Column(name = "key", nullable = false, unique = true)
    var key: String? = null

    @Column(name = "lat", nullable = false, unique = true)
    var lat: Double? = null

    @Column(name = "long", nullable = false, unique = true)
    var long: Double? = null

    @Column(name = "updated_at", nullable = false, unique = true)
    var updatedAt: Date? = null

    override fun toString(): String {
        return "${super.toString()} : ${Converter.toString(this)}"
    }
}