package com.server.mkrworld.gpslocator.db.repo

import com.server.mkrworld.gpslocator.db.entity.GpsLocation
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * @author THEMKR
 * Repo of the GpsLocation Table
 */
@Repository
interface GpsLocationRepo : JpaRepository<GpsLocation, String> {

}