package com.server.mkrworld.gpslocator.annotation

import com.server.mkrworld.gpslocator.utils.Constants

@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
annotation class API(val gatewayType: Constants.GatewayType, val requestType: Constants.RequestType, val requestPath: String)