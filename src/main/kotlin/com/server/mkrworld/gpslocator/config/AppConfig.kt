package com.server.mkrworld.gpslocator.config

import com.mkrworld.server.gpslocator.server.HttpServerVerticle
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["com.server.mkrworld.gpslocator"])
class AppConfig {

    private val log: Logger = LoggerFactory.getLogger(AppConfig::class.java)

    @Value("\${gpslocator.vertx.server.port}")
    var vertxPort: Int? = null

    /**
     * Method to init the HttpServerVerticle Bean
     *
     * @return HttpServerVerticle Bean
     */
    @Bean
    fun initHttpServerVerticle(): HttpServerVerticle {
        return HttpServerVerticle()
    }
}