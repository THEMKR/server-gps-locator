package com.server.mkrworld.gpslocator.config

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.PlatformTransactionManager
import java.util.*
import javax.annotation.PostConstruct
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource

@Configuration
@EnableJpaRepositories(basePackages = ["com.server.mkrworld.gpslocator"])
class DataBaseConfig {

    private val log: Logger = LoggerFactory.getLogger(DataBaseConfig::class.java)

    @Value("\${gpslocator.db.url}")
    private var dbUrl: String? = null

    @Value("\${gpslocator.db.username}")
    private var userName: String? = null

    @Value("\${gpslocator.db.password}")
    private var password: String? = null

    @Value("\${gpslocator.db.driver.class.name}")
    private var jdbcDriverClass: String? = null

    @Value("\${gpslocator.db.connection.time.out}")
    private var connectionTimeOut: Long? = null

    @Value("\${gpslocator.db.idle.time.out}")
    private var idleTimeOut: Long? = null

    @Value("\${gpslocator.db.max.pool.size}")
    private var maxPoolSize: Int? = null

    @Value("\${gpslocator.db.orm.dialect}")
    private var hibernateDialect: String? = null

    @Value("\${gpslocator.db.orm.ddl-auto}")
    private var schemaDDL: String? = null

    @Value("\${gpslocator.db.min.idle}")
    private var minimumIdle: Int? = null

    @Value("\${gpslocator.db.max.life.time.ms}")
    private var maxLifeTime: Long? = null

    /**
     * Entity manager factory local container entity manager factory bean.
     *
     * @return the local container entity manager factory bean
     */
    @Bean
    fun entityManagerFactory(): LocalContainerEntityManagerFactoryBean {
        val em = LocalContainerEntityManagerFactoryBean()
        em.dataSource = dataSource()
        // SCAN ENTITY CLASS REPO
        em.setPackagesToScan("com.server.mkrworld")
        em.jpaVendorAdapter = HibernateJpaVendorAdapter()
        em.setJpaProperties(additionalProperties())
        return em
    }

    /**
     * Data source data source.
     *
     * @return the data source
     */
    @Bean
    fun dataSource(): DataSource {
        val config = HikariConfig()
        config.poolName = "SpringBootJPAHikariCP"
        config.jdbcUrl = dbUrl
        config.username = userName
        config.password = password
        config.connectionTimeout = connectionTimeOut!!
        config.driverClassName = jdbcDriverClass
        config.idleTimeout = idleTimeOut!!
        config.maximumPoolSize = maxPoolSize!!
        config.minimumIdle = minimumIdle!!
        config.maxLifetime = maxLifeTime!!
        return HikariDataSource(config)
    }

    /**
     * Transaction manager platform transaction manager.
     *
     * @param emf the emf
     * @return the platform transaction manager
     */
    @Bean
    fun transactionManager(emf: EntityManagerFactory?): PlatformTransactionManager {
        val transactionManager = JpaTransactionManager()
        transactionManager.entityManagerFactory = emf
        return transactionManager
    }

    /**
     * Exception translation persistence exception translation post processor.
     *
     * @return the persistence exception translation post processor
     */
    @Bean
    fun exceptionTranslation(): PersistenceExceptionTranslationPostProcessor {
        return PersistenceExceptionTranslationPostProcessor()
    }

    /**
     * Additional properties properties.
     *
     * @return the properties
     */
    fun additionalProperties(): Properties {
        val properties = Properties()
        properties.setProperty("hibernate.hbm2ddl.auto", schemaDDL)
        properties.setProperty("hibernate.dialect", hibernateDialect)
        properties.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false")
        return properties
    }

    @PostConstruct
    fun init() {
        log.info("db_url {}", dbUrl)
    }
}