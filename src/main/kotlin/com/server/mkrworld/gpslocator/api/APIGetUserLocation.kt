package com.server.mkrworld.gpslocator.api

import com.server.mkrworld.gpslocator.annotation.API
import com.server.mkrworld.gpslocator.contract.BaseRequestHandler
import com.server.mkrworld.gpslocator.contract.BaseResponse
import com.server.mkrworld.gpslocator.db.DataBaseProvider
import com.server.mkrworld.gpslocator.db.entity.GpsLocation
import com.server.mkrworld.gpslocator.utils.Constants
import com.server.mkrworld.gpslocator.utils.UrlEndpoint
import io.vertx.core.http.HttpServerRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import java.util.*

@Service
@API(Constants.GatewayType.VERTX, Constants.RequestType.GET, UrlEndpoint.VertX.GET_GPS_LOCATION)
class APIGetUserLocation : BaseRequestHandler<HttpServerRequest> {

    @Autowired
    private var dataBase: DataBaseProvider? = null

    override fun handleRequest(request: HttpServerRequest?): BaseResponse<*> {
        val id = request?.params()?.get("id") ?: ""
        val userLocation: GpsLocation? = dataBase?.gpsLocationRepo?.findById(id)?.get()
        println("USER LOCATION FETCHED FROM DB for KEY : $id :: DATA  : $userLocation")
        return if (Objects.nonNull(userLocation)) {
            getSuccessResponse(userLocation!!)
        } else {
            getFailureResponse()
        }
    }

    /**
     * Method to get the successful response
     * @param userLocation
     */
    private fun getSuccessResponse(userLocation: GpsLocation): BaseResponse<GpsLocation> {
        val baseResponse = BaseResponse<GpsLocation>()
        baseResponse.payload = userLocation
        baseResponse.isSuccess = true
        baseResponse.status = HttpStatus.OK.value()
        baseResponse.message = "User location fetched successfully";
        return baseResponse
    }

    /**
     * Method to get the failure response
     */
    private fun getFailureResponse(): BaseResponse<GpsLocation> {
        val baseResponse = BaseResponse<GpsLocation>()
        baseResponse.isSuccess = false
        baseResponse.status = HttpStatus.OK.value()
        baseResponse.message = "Unable to found user data";
        return baseResponse
    }
}