package com.server.mkrworld.gpslocator.api

import com.server.mkrworld.gpslocator.annotation.API
import com.server.mkrworld.gpslocator.contract.BaseRequestHandler
import com.server.mkrworld.gpslocator.contract.BaseResponse
import com.server.mkrworld.gpslocator.exception.InitializeException
import com.server.mkrworld.gpslocator.utils.Constants
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServerRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.annotation.PostConstruct
import kotlin.collections.HashMap

/**
 * @author THEMKR
 * Gateway class to handle and validate all the incoming request
 */
@Service
class Router {

    companion object {
        private val LOGGER: Logger = LoggerFactory.getLogger(Router::class.java)
    }

    @Autowired
    private var apiList: List<BaseRequestHandler<*>>? = null

    private val map = HashMap<String, BaseRequestHandler<*>>()

    @PostConstruct
    private fun init() {
        apiList?.iterator()?.forEach { baseRequest ->
            val annotation = Optional.ofNullable(baseRequest.javaClass.getAnnotation(API::class.java))
            if (annotation.isPresent) {
                val key = keyGenerator(annotation.get().gatewayType, annotation.get().requestType, annotation.get().requestPath)
                if (map.containsKey(key)) {
                    val message = String.format(Constants.ErrorMessage.MULTIPLE_API_SUBSCRIBER_S2.message, map[key]!!::class.java.canonicalName, baseRequest::class.java.canonicalName)
                    LOGGER.error(message).toString()
                    throw InitializeException(message)
                }
                map[key] = baseRequest
            }
        }
    }

    /**
     * Method to handle the request received by the VertX Server
     * @param request
     */
    fun vertXRequest(request: HttpServerRequest): BaseResponse<*> {
        val key = keyGenerator(Constants.GatewayType.VERTX, when (request.method()) {
            HttpMethod.GET -> {
                Constants.RequestType.GET
            }
            HttpMethod.PUT -> {
                Constants.RequestType.PUT
            }
            else -> {
                Constants.RequestType.GET
            }
        }, request.path())
        if (!map.containsKey(key)) {
            val message = String.format(Constants.ErrorMessage.API_NOT_FOUND_S1.message, key)
            LOGGER.error(message).toString()
            throw InitializeException(message)
        }
        return (map[key] as BaseRequestHandler<Any>).handleRequest(request)
    }

    /**
     * Method to generate the key based on the Param
     */
    private fun keyGenerator(gatewayType: Constants.GatewayType, requestType: Constants.RequestType, requestPath: String): String {
        return "${gatewayType}->${requestType}->${requestPath}"
    }
}