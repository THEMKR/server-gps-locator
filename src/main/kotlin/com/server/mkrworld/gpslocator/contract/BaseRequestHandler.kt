package com.server.mkrworld.gpslocator.contract

/**
 * @author THEMKR
 * Contract for all request handler VertXServer.
 * All the implementor of this must Annotate @API Annotation at the time of Implementing it
 */
interface BaseRequestHandler<MKR> {

    /**
     * Method called from server/controller class to handle the request obj pass in param
     * @param request
     */
    fun handleRequest(request: MKR?): BaseResponse<*>
}