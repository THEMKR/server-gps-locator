package com.server.mkrworld.gpslocator.contract

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * @author THEMKR
 * Contract for all Gateway supported by VertXServer
 */
class BaseResponse<MKR> {

    @JsonProperty("status")
    var status: Int = 500

    @JsonProperty("success")
    var isSuccess: Boolean = false

    @JsonProperty("message")
    var message: String = ""

    @JsonProperty("error")
    var error: ArrayList<String> = ArrayList()

    @JsonProperty("data")
    var payload: MKR? = null
}